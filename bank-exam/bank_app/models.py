from django.db import models

# Create your models here.

class User(models.Model):
    user_id = models.Field(primary_key = True)


class Account(models.Model):
   account_id = models.CharField(max_length=200)
   user_id = models.ForeignKey(User, on_delete=models.CASCADE)
   
   ACCOUNT_TYPE = (
    ("Silver", "Silver"),
    ("Gold", "Gold"),
    ("Platinum", "Platinum"),
)

   account_type = models.CharField(
        max_length = 20,
        choices = ACCOUNT_TYPE,
        default = '1'
        )
   


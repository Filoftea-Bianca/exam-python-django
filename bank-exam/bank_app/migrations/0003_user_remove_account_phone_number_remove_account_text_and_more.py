# Generated by Django 4.0.3 on 2022-03-11 13:48

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('bank_app', '0002_account_phone_number'),
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('user_id', models.Field(primary_key=True, serialize=False)),
            ],
        ),
        migrations.RemoveField(
            model_name='account',
            name='phone_number',
        ),
        migrations.RemoveField(
            model_name='account',
            name='text',
        ),
        migrations.AddField(
            model_name='account',
            name='account_id',
            field=models.CharField(default=1, max_length=200),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='account',
            name='account_type',
            field=models.CharField(choices=[('Silver', 'Silver'), ('Gold', 'Gold'), ('Platinum', 'Platinum')], default='1', max_length=20),
        ),
        migrations.AddField(
            model_name='account',
            name='user_id',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='bank_app.user'),
            preserve_default=False,
        ),
    ]

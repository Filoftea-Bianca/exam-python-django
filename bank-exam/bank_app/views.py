from django.shortcuts import render
from .models import Account


def index(request):
   accounts = Account.objects.all()
   context = {
      'accounts': accounts
   }
   return render(request, 'bank_app/index.html', context)
